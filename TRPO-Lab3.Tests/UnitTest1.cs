using NUnit.Framework;
using System;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [Test]
        public void TestSharSegment()
        {
            double R = 1;
            double h = 100;
            var expected = 628;
            var S =  TRPO_Lab3.Lib.Functions.SharSegment(R, h);
            Assert.AreEqual(expected, S);
            
        }

        

        [Test]
        public void TestResults()
        {
            double R = 1;
            double h = 100;
            double S = TRPO_Lab3.Lib.Functions.SharSegment(R, h);
            if (S<=0)
            {
                Assert.Fail("����� ������ �� ����������!");
            }
            else
            {
                Assert.Pass("�������!");
            }
           
        }
    }

}