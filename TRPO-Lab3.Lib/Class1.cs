﻿using System;



namespace TRPO_Lab3.Lib
{
    public class Functions
    {
        public static double SharSegment(double R, double h)
        {
            const double pi = 3.14;
            double S = 2.0 * pi * R * h;
            return S;
        }
        

    }
}
